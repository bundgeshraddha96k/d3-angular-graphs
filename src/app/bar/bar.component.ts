import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css'],
})
export class BarComponent implements OnInit,AfterViewInit {
  @ViewChild('ref')
  barRef!: ElementRef;
  @Input() paramsData : any
  @Input() plotData:any
  
  private svg: any;
 
  constructor() {}

  ngOnInit(): void {
    
  }
  ngAfterViewInit(){
    this.createSvg(this.barRef.nativeElement);
    this.drawBars(this.plotData);
  }
  private createSvg(ele:any): void {

    this.svg = d3
      .select(ele)
      .append('svg')
      .attr('width', this.paramsData.width + this.paramsData.margin.right + this.paramsData.margin.left * 2)
      .attr('height', this.paramsData.height + this.paramsData.margin.top + this.paramsData.margin.bottom  * 2)
      .append('g')
      .attr('transform', 'translate(' + this.paramsData.margin.right + ',' + this.paramsData.margin.bottom + ')')
      .attr('position', 'relative')
  }
  
  
  private drawBars(data: any[]): void {
    const x = d3
      .scaleBand()
      .range([0, this.paramsData.width])
      .domain(data.map((d) => {
        
        return d.Framework}))
      .padding(0.2);

    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + this.paramsData.height + ')')
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .style('text-anchor', 'end');
    var yMax = Math.max(...this.plotData.map((o:any) => parseInt(o.Stars) ))    
    
    const y = d3.scaleLinear().domain([0, yMax]).range([this.paramsData.height, 0]);

    this.svg.append('g').call(d3.axisLeft(y)).attr('position', 'absolute').call((g:any) => g.select(".domain").remove())
    
    this.svg
      .selectAll('bars')
      .data(data)
      .enter()
      .append('rect')
      .attr('x', (d: any) => x(d.Framework))
      .attr('y', (d: any) => y(d.Stars))
      .attr('width', x.bandwidth())
      .attr('height', (d: any) => this.paramsData.height - y(d.Stars))
      .attr('fill', '#d04a35');
  }
}
