import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import {paramsData,barData ,stackedBarDat,LineData} from '../model/interfaces'

@Component({
  selector: 'app-plots',
  templateUrl: './plots.component.html',
  styleUrls: ['./plots.component.css']
})
export class PlotsComponent implements OnInit, AfterViewInit {
  @ViewChild("ref")stackedBar!: ElementRef;
  tooltip:any
  paramsData : paramsData = {
    width : 750,
    height : 400,
    eventEffect: false,
    margin: {
      top : 50,
      right: 50,
      bottom: 50,
      left: 50,
    },
  }
  

  public plotData :Array<barData> = [
    { Framework: 'Vue', Stars: '166443', Released: '2014' },
    { Framework: 'React', Stars: '150793', Released: '2013' },
    { Framework: 'Angular', Stars: '62342', Released: '2016' },
    { Framework: 'Backbone', Stars: '27647', Released: '2010' },
    { Framework: 'Ember', Stars: '21471', Released: '2011' },
  ];

  public stackedData :Array<stackedBarDat>= [
    { month: 'Jan', apples: 3840, bananas: 1920, cherries: 960, dates: 400 },
    { month: 'Feb', apples: 1600, bananas: 1440, cherries: 960, dates: 400 },
    { month: 'March', apples: 640, bananas: 960, cherries: 640, dates: 400 },
    { month: 'Apr', apples: 3120, bananas: 1480, cherries: 640, dates: 400 },
  ];

  linarPlotData :Array<LineData> = [
    { date: '01/01/2016', pizzas: 10000 },
    { date: '01/02/2016', pizzas: 20000 },
    { date: '01/03/2016', pizzas: 40000 },
    { date: '01/04/2016', pizzas: 30000 },
    { date: '01/05/2016', pizzas: 30000 },
    { date: '01/06/2016', pizzas: 50000 },
    { date: '01/07/2016', pizzas: 30000 },
    { date: '01/08/2016', pizzas: 50000 },
    { date: '01/09/2016', pizzas: 60000 },
    { date: '01/10/2016', pizzas: 20000 },
    { date: '01/11/2016', pizzas: 10000 },
    { date: '01/12/2016', pizzas: 90000 },
  ];

  constructor() { }
 

  ngOnInit(): void {

  }
  ngAfterViewInit(){
    console.log("this.lineref.nativeElement", this.stackedBar.nativeElement);
    
    this.tooltip =d3
    .select(this.stackedBar.nativeElement)
    .append('div')
    .style('opacity', 0)
    .attr('class', 'tooltip')
    .style('background-color', 'white')
    .style('border', 'solid')
    .style('border-width', '1px')
    .style('border-radius', '5px')
    .style('position', 'absolute')
    .style('padding', '10px')
    .style('width', '150px')
    
  }

  mousemove(d:any){
   
    this.tooltip.html("month is " + d.path[0].__data__.data['month'] +"<br>" +  "data is " + (d.path[0].__data__[1] - d.path[0].__data__[0] + "<br>" + "fruit is " + d.path[1].__data__.key))
      .style('position', 'absolute')
      .style('left', d.screenX+10+"px" ) 
      .style('top', d.screenY+10+"px" )
  }
  mouseover(){
    this.tooltip.style('opacity', 1);
  }
  mouseleave(){
    this.tooltip.transition().duration(200).style('opacity', 0);
  }
  eventOperation(d:any){
    
    if(d.type == 'mousemove'){
      this.mouseover()
    }
    if(d.type == 'mouseover'){
      this.mousemove(d)
    }
    
    if(d.type == 'mouseleave'){
      this.mouseleave()
    }
  }

}
