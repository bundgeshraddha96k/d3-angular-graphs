import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-bar-plot',
  templateUrl: './bar-plot.component.html',
  styleUrls: ['./bar-plot.component.css'],
})
export class BarPlotComponent implements OnInit,AfterViewInit {
  @ViewChild("ref")
  barPlotRef!: ElementRef;
  @Input() paramsData : any
  @Input() stackedData:any
  @Output() transEmitter = new EventEmitter()
  private svg: any;
  
  constructor() {}

  ngOnInit(): void {
   
    
  }
  ngAfterViewInit(){
    this.createSvg(this.barPlotRef.nativeElement);//this.barPlotRef.nativeElement
    this.drawBars(this.stackedData);
    
    
  }

  private createSvg(el:any) {
    this.svg = d3
      .select(el)
      .append('svg')
      .attr('width', this.paramsData.width + this.paramsData.margin.left + this.paramsData.margin.right)
      .attr('height', this.paramsData.height + this.paramsData.margin.top + this.paramsData.margin.bottom)
      .attr('position', 'relative')
      .append('g')
      .attr('transform', `translate(${this.paramsData.margin.left},${this.paramsData.margin.top})`)
  }
  

 

  private drawBars(data: any[]): void {
    
    const fruit = Object.keys(data[0]).filter((d) => d != 'month');
    const months = data.map((d) => d.month);

    const stackedData = d3.stack().keys(fruit)(data);
    
    const x = d3.scaleLinear().domain([0, 7120]).range([0, this.paramsData.width]);

    const y = d3
      .scaleBand()
      .domain(months)
      .range([0, this.paramsData.height])
      .padding(0.25);

    const color = d3.scaleOrdinal().domain(fruit).range(d3.schemeTableau10);
    const xAxis = d3.axisBottom(x).ticks(5, '~s');
    const yAxis = d3.axisLeft(y);
   
    this.svg
      .append('g')
      .attr('transform', `translate(0,${this.paramsData.height})`)
      .call(xAxis)
      .call((g: any) => g.select('.domain').remove());

    this.svg
      .append('g')
      .call(yAxis)
      .call((g: any) => g.select('.domain').remove());


    this.svg
      .append('g')
      .selectAll('g')
      .data(stackedData)
      .enter()
      .append('g')
      .attr('fill', (d: any) => color(d.key))
      .selectAll('rect')
      .data((d: any) => d)
      .enter()
      .append('rect')
      .attr('x', (d: any) => {
        return x(d[0]);
      })
      .attr('y', (d: any) => y(d.data.month))
      .attr('height', y.bandwidth())
      .attr('width', (d: any) => x(d[1]) - x(d[0]))
    .on('mouseover',(d:any) =>{this.transEmitter.emit(d)})
    .on('mousemove',(d:any)=> this.transEmitter.emit(d))
    .on('mouseleave',(d:any) =>this.transEmitter.emit(d));
  }
 
}
