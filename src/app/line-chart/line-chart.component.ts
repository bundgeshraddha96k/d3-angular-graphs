import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css'],
})

export class LineChartComponent implements OnInit,AfterViewInit {
  @ViewChild("ref")
  lineChartRef!: ElementRef;
  @Input() paramsData : any
  @Input() linarPlotData:any
  
  private svg: any;
 
  constructor() {}

  ngOnInit(): void {
    
  }
  ngAfterViewInit(){
    
    this.createSvg(this.lineChartRef.nativeElement);
    this.drawBars(this.linarPlotData);
  }
  private createSvg(ele:any) {

    this.svg = d3
      .select(ele)
      .append('svg')
      .attr('fill', '#d04a35')
      .attr('width', this.paramsData.width + this.paramsData.margin.left + this.paramsData.margin.right)
      .attr('height', this.paramsData.height + this.paramsData.margin.bottom + this.paramsData.margin.bottom)
      .attr('position', 'relative')
      .append('g')
      .attr("transform", "translate(" + this.paramsData.margin.left + "," + this.paramsData.margin.top + ")");
  }

  private drawBars(data: any[]): void {

    

    var parseTime :any = d3.timeParse("%d/%m/%Y");
    
    let x = d3
      .scaleTime()
      .range([0, this.paramsData.width]);
   
      
    const y = d3.scaleLinear().range([this.paramsData.height, 0]);
   
    x.domain(<[Date,Date]>d3.extent(data, function(d){
      
      return parseTime(d.date)
    }))
    y.domain(<any>d3.extent(data,function(d){
      return d.pizzas/1000
    }))
    
    this.svg
      .append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.paramsData.height + ')')
      .call(d3.axisBottom(x))
      // .tickFormat(d3.timeFormat("%b"));

    this.svg.append('g').attr('class', 'axis axis--y').call(d3.axisLeft(y));

    this.svg
      .append('text')
      .attr('x', this.paramsData.width / 2)
      .attr('y', 20 - this.paramsData.margin.top / 2)
      .attr('text-anchor', 'middle')
      .style('font-size', '16px')
      .text('Pizza consumption');

    this.svg
      .append('path')
      .datum(data)
      .attr('position', 'absolute')
      .attr(
        'd',
        d3.line<any>()
          .x((d:any):any=>{
            return x(parseTime(d.date))
          })
          .y((d: any) => {
            return y(d.pizzas / 1000);
          })
      )
      .attr('stroke', 'blue')
      .style('stroke-width', 4)
      .style('fill', 'none');
  }
}
