
export interface LineData{
  
    date : String,
    pizzas: number
  
}
export interface barData{
    // Framework: 'Vue', Stars: '166443', Released: '2014'
    Framework: String,
    Stars : String|number,
    Released: String|number
}
export interface stackedBarDat{
    // { month: 'Jan', apples: 3840, bananas: 1920, cherries: 960, dates: 400 }
    month : String,
    apples : number,
    bananas :number,
    cherries: number,
    dates :number
}
export interface paramsData{
    width: number,
    height: number,
    margin: {
        top : number,
        right: number,
        bottom: number,
        left: number
    },
    eventEffect: boolean,
   
}


